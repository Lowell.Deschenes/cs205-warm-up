# this file will hold the functions for creating the tables and populating them

import sqlite3
from sqlite3 import Error
import pandas as pd

#first we must create a connection to a database file
def create_connection():
    """
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(r"us_historians.db")
        return conn
    except Error as e:
        print(e)

    return conn

def create_cursor(conn):
    """ create a cursor
    :param conn: Connection object
    """
    c = conn.cursor()
    return c

def make_table(conn,table):
    """ create a table
    :param conn: Connection object
    :param table: table we want to create
    """
    try:
        c = create_cursor(conn)
        c.execute(table)
    except Error as e:
        print(e)

    #commit the changes to the db
    conn.commit()

def populate_table(conn,csv,table_title):
    """ populate a table
    :param conn: Connection object
    :param csv: csv we wish to populate table with
    :param table_title: the title of the table we want to populate
    """
    # load data into a pandas dataframe
    data = pd.read_csv(csv)
    # write the data to the connected sql database and table
    data.to_sql(table_title, conn, if_exists='append', index=False)

    #commit the changes to the db
    conn.commit()


def create_table(conn):
    #create the president and vice president tables
    
    presidents_table = """ CREATE TABLE IF NOT EXISTS Presidents (
                                        number integer PRIMARY KEY,
                                        name text NOT NULL,
                                        birth_year integer NOT NULL,
                                        term_begin integer NOT NULL,
                                        term_end integer,
                                        home_state text NOT NULL,
                                        political_party text
                                    ); """

    v_president_table = """CREATE TABLE IF NOT EXISTS Vice_Presidents (
                                        name text NOT NULL,
                                        number integer PRIMARY KEY,
                                        term_begin integer NOT NULL,
                                        term_end integer,
                                        political_party text,
                                        presidents_number integer NOT NULL,
                                        FOREIGN KEY (presidents_number) REFERENCES Presidents(number)
                                );"""

    #make sure the database does not already exist
    if existence_check(conn) == False:
        #make the tables
        make_table(conn, presidents_table)
        make_table(conn, v_president_table)
        #populate the tables
        populate_table(conn, 'presidents.csv', "Presidents")
        populate_table(conn,'vice-president.csv', "Vice_Presidents")

def close_connection(conn):
    """ close connection
        :param conn: Connection object
        """
    conn.close()
#once i am done doing stuff with the database I should close the connection conn.close()

def existence_check(conn):
    """ close connection
          :param conn: Connection object
          :return: boolean of whether the tables exist or not
          """
    #create a cursor for connection
    c = conn.cursor()
    #get count of tables with name = Presidents
    c.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='Presidents' ''')
    if c.fetchone()[0] == 1:
        return True
    else:
        return False
