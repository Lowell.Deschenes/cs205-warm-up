# us_historians - Lowell Deschenes, Scotti Day, Sean Applegate

# imports
import tables
import parser
import queries
import sys


# main function
def main():
    # make the exit variable
    loop = True
    database_exist = False

    # create connection to database and check to see if data has been loaded
    conn = tables.create_connection()
    c = tables.create_cursor(conn)

    if tables.existence_check(conn):
        # check before query to see if exists, if doesn't tell user to load the data before doing query
        database_exist = True

    # start the while loop for the parser
    while loop:

        # quit function
        query = parser.create_parser(conn)
        if query == 'exit':
            sys.exit()

        # help function
        if query == 'help':
            print('vice president [PRESIDENT NAME] \npresident [VICE PRESIDENT NAME]'
                  '\nborn [PRESIDENT NAME] \npolitical party [NAME] \nyear took office [NAME]'
                  '\nstate [PRESIDENT NAME] \nenter exit to end query')

        # load data function
        if query == 'load data':
            if database_exist:
                print('Data already loaded')
            else:
                database_exist = True

        # splitting query function
        if database_exist:
            try:
                if query == 'quotation error':

                    print("missing closing quotation")

                if query == 'short query':
                    print("first term value error. enter help for proper query structures")

                if query == 'long query':
                    print("query too long. Remember to wrap names in quotation marks. enter help for proper query "
                          "structures")

                if query == 'first term value error':
                    print('first command does not match valid input. enter help for proper query structures')

                if query == 'second term value error':
                    print('second command does not match valid input. enter help for proper query structures')

                if query == 'third term value error':
                    print('third command does not match valid input. enter help for proper query structures')

                if 'vice' in query and 'president' in query:
                    queries.get_vice_president(query[2], conn)

                if 'president' in query and 'vice' not in query:
                    queries.get_president(query[1], conn)

                if 'party' in query and 'political' in query:
                    queries.get_party(query[2], conn)

                if 'state' in query:
                    queries.get_state(query[1], conn)

                if 'born' in query:
                    queries.get_birth(query[1], conn)

                if 'year' in query and 'took' in query:
                    queries.get_term_begin(query[3], conn)

                if 'year' in query and 'left' in query:
                    queries.get_term_end(query[3], conn)
            except IndexError:
                print("No name provided")

        else:
            if query != 'help':
                print("No data loaded. Enter the command 'load data'")


if __name__ == '__main__':
    main()
