import tables


def get_vice_president(query_name, conn):
    c = tables.create_cursor(conn)
    c.execute("SELECT name FROM Vice_Presidents WHERE presidents_number = ( SELECT number FROM Presidents WHERE name=?)", (query_name,))
    result = c.fetchall()
    if result:
        for i in range(0, len(result)):
            print(result[i][0])
    else:
        # check if president exists
        c.execute("SELECT number FROM Presidents WHERE name=?", (query_name,))
        num = c.fetchone()
        if num:
            print("No vice president")
        else:
            print("Name Error: Invalid name entered")


def get_president(query_name, conn):
    c = tables.create_cursor(conn)
    c.execute("SELECT name FROM Presidents WHERE number = ( SELECT presidents_number FROM Vice_Presidents WHERE name=?)", (query_name,))
    result = c.fetchall()
    if result:
        for i in range(0, len(result)):
            print(result[i][0])
    else:
        print("Name Error: Invalid name entered")


def get_party(query_name, conn):
    c = tables.create_cursor(conn)
    c.execute("SELECT political_party FROM Presidents WHERE name=?", (query_name,))
    p_party = c.fetchone()
    c.execute("SELECT political_party FROM Vice_Presidents WHERE name=?", (query_name,))
    vp_party = c.fetchone()

    if p_party and vp_party and p_party[0] != vp_party[0]:
        print('Party while President:', p_party[0])
        print('Party while Vice President:', vp_party[0])
    elif p_party:
        print(p_party[0])
    elif vp_party:
        print(vp_party[0])
    else:
        print("Name Error: Invalid name entered")


def get_state(query_name, conn):
    c = tables.create_cursor(conn)
    c.execute("SELECT home_state FROM Presidents WHERE name=?", (query_name,))
    state = c.fetchone()
    if state:
        print(state[0])
    else:
        print("Name Error: Invalid name entered")


def get_birth(query_name, conn):
    c = tables.create_cursor(conn)
    c.execute("SELECT birth_year FROM Presidents WHERE name = ?", (query_name,))
    birth = c.fetchone()
    if birth:
        print(birth[0])
    else:
        print("Name Error: Invalid name entered")


# Add if both president and vice president
# If president
def get_term_begin(query_name, conn):
    c = tables.create_cursor(conn)
    c.execute("SELECT term_begin FROM Presidents WHERE name = ?", (query_name,))
    p_begin_term = c.fetchall()
    c.execute("SELECT term_begin FROM Vice_Presidents WHERE name = ?", (query_name,))
    vp_begin_term = c.fetchall()

    if vp_begin_term:
        if len(vp_begin_term) == 1:
            print('Became vice president in', vp_begin_term[0][0])
        else:
            print('Became vice president in', vp_begin_term[0][0], 'and', vp_begin_term[1][0])

    if p_begin_term:
        if len(p_begin_term) == 1:
            print('Became president in', p_begin_term[0][0])
        else:
            print('Became president in', p_begin_term[0][0], 'and', p_begin_term[1][0])

    if not p_begin_term and not vp_begin_term:
        print("Name Error: Invalid name entered")

def get_term_end(query_name, conn):
    c = tables.create_cursor(conn)
    c.execute("SELECT term_end FROM Presidents WHERE name = ?", (query_name,))
    p_end_term = c.fetchall()
    c.execute("SELECT term_end FROM Vice_Presidents WHERE name = ?", (query_name,))
    vp_end_term = c.fetchall()

    if vp_end_term:
        if len(vp_end_term) == 1:
            print('Left vice presidential office in', vp_end_term[0][0])
        else:
            print('Left vice presidential office in', vp_end_term[0][0], 'and', vp_end_term[1][0])

    if p_end_term:
        if len(p_end_term) == 1:
            print('Left presidential office in', p_end_term[0][0])
        else:
            print('Left presidential office in', p_end_term[0][0], 'and', p_end_term[1][0])

    if not p_end_term and not vp_end_term:
        print("Name Error: Invalid name entered")
