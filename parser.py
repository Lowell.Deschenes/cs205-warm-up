# this file creates the parser
# imports
import tables
import shlex

QUERY_INDEX_0_LIST = ['load', 'vice', 'political', 'year', 'exit', 'help', 'president', 'born', 'state' ]
QUERY_INDEX_1_LIST = ['data', 'president', 'party', 'took', 'left']
QUERY_INDEX_2_LIST = ['office']


# create the parser function
def create_parser(conn):
    # create the query input
    query = input('>')

    # make the quit function
    if query == 'exit':
        # close the connection once you exit
        try:
            tables.close_connection(conn)
        finally:
            return query

    # make the help function
    if query == 'help':
        return 'help'

    # make the load data function
    if query == 'load data':
        tables.create_table(conn)
        return query

    # try to split the input and catch the error
    try:
        query = shlex.split(query)
    except:
        return 'quotation error'

    if len(query) < 2 and (query != 'help' or query != 'quit'):
        return "short query"

    if len(query) > 4:
        return "long query"

    if len(query) > 1:
        if query[0] in QUERY_INDEX_0_LIST:
            ind_1 = QUERY_INDEX_0_LIST.index(query[0])
        if query[1] in QUERY_INDEX_1_LIST:
            ind_2 = QUERY_INDEX_1_LIST.index(query[1])

        if query[0] not in QUERY_INDEX_0_LIST:
            return 'first term value error'

        if query[1] in QUERY_INDEX_1_LIST and ind_1 != ind_2:
            return 'second term value error'


    if len(query) > 2:
        if query[0] in QUERY_INDEX_0_LIST:
            ind_3 = QUERY_INDEX_0_LIST.index(query[0])
        if query[1] in QUERY_INDEX_1_LIST:
            ind_4 = QUERY_INDEX_1_LIST.index(query[1])

        if query[1] not in QUERY_INDEX_0_LIST:
            return 'second term value error'

        if query[1] in QUERY_INDEX_1_LIST and ind_3 != ind_4:
            if query[1] != 'left' and len(query) < 3:
                return 'second term value error'

    if len(query) > 3:
        if query[2] not in QUERY_INDEX_2_LIST:
            print(query)
            return 'third term value error'

    return query







